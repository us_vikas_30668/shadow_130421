
package com.example.shadow_api_130421.data.models.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LoginDepot {

    @SerializedName("msg")
    @Expose
    private String msg;
    @SerializedName("depotName")
    @Expose
    private String depotName;
    @SerializedName("flag")
    @Expose
    private Integer flag;
    @SerializedName("roleid")
    @Expose
    private Integer roleid;
    @SerializedName("rolename")
    @Expose
    private String rolename;
    @SerializedName("isactive")
    @Expose
    private String isactive;
    @SerializedName("depotId")
    @Expose
    private Integer depotId;
    @SerializedName("agencyId")
    @Expose
    private Integer agencyId;
    @SerializedName("cityId")
    @Expose
    private Integer cityId;
    @SerializedName("phoneno")
    @Expose
    private Object phoneno;
    @SerializedName("agencyName")
    @Expose
    private String agencyName;
    @SerializedName("cityName")
    @Expose
    private String cityName;
    @SerializedName("displayname")
    @Expose
    private String displayname;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("username")
    @Expose
    private String username;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getDepotName() {
        return depotName;
    }

    public void setDepotName(String depotName) {
        this.depotName = depotName;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }

    public Integer getDepotId() {
        return depotId;
    }

    public void setDepotId(Integer depotId) {
        this.depotId = depotId;
    }

    public Integer getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Integer agencyId) {
        this.agencyId = agencyId;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Object getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(Object phoneno) {
        this.phoneno = phoneno;
    }

    public String getAgencyName() {
        return agencyName;
    }

    public void setAgencyName(String agencyName) {
        this.agencyName = agencyName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getDisplayname() {
        return displayname;
    }

    public void setDisplayname(String displayname) {
        this.displayname = displayname;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

}
