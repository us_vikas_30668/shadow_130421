package com.example.shadow_api_130421.data.models.service;

public class FlatFareMasters {

    private int flatFareId;

    private String flatFareName;

    private String flatFareCode;

    private double flatFareAmount;

    private long createdOn;

    public int getFlatFareId() {
        return flatFareId;
    }

    public String getFlatFareName() {
        return flatFareName;
    }

    public String getFlatFareCode() {
        return flatFareCode;
    }

    public double getFlatFareAmount() {
        return flatFareAmount;
    }

    public long getCreatedOn() {
        return createdOn;
    }
}
