package com.example.shadow_api_130421.data.models.service;

public class TicketTypes {

    private int ticketId;

    private String ticketCode;

    private String ticketDesc;

    private long createdOn;

    public int getTicketId() {
        return ticketId;
    }

    public String getTicketCode() {
        return ticketCode;
    }

    public String getTicketDesc() {
        return ticketDesc;
    }

    public long getCreatedOn() {
        return createdOn;
    }
}
