package com.example.shadow_api_130421.data.models.service;

public class ConcessionMasters {

    private int concessionId;

    private String concessionCode;

    private String concessionName;

    private String concessionType;

    private double discountPercentage;

    private int maxAdult;

    private int maxChild;

    private long createdOn;

    public int getConcessionId() {
        return concessionId;
    }

    public String getConcessionCode() {
        return concessionCode;
    }

    public String getConcessionName() {
        return concessionName;
    }

    public String getConcessionType() {
        return concessionType;
    }

    public double getDiscountPercentage() {
        return discountPercentage;
    }

    public int getMaxAdult() {
        return maxAdult;
    }

    public int getMaxChild() {
        return maxChild;
    }

    public long getCreatedOn() {
        return createdOn;
    }
}
