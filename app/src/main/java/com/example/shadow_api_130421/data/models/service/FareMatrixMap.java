package com.example.shadow_api_130421.data.models.service;

import java.util.List;

public class FareMatrixMap {

    private List<Integer> standardAdultFare;

    private List<Integer> standardSeniorCitizenFare;

    private List<Integer> standardChildrenFare;

    public List<Integer> getStandardAdultFare() {
        return standardAdultFare;
    }

    public List<Integer> getStandardSeniorCitizenFare() {
        return standardSeniorCitizenFare;
    }

    public List<Integer> getStandardChildrenFare() {
        return standardChildrenFare;
    }
}
