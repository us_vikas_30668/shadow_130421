
package com.example.shadow_api_130421.data.models.service4;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TripMaster {

    @SerializedName("tripId")
    @Expose
    private Integer tripId;
    @SerializedName("tripNo")
    @Expose
    private Integer tripNo;
    @SerializedName("tripDirection")
    @Expose
    private String tripDirection;
    @SerializedName("tripStartTime")
    @Expose
    private long tripStartTime;
    @SerializedName("tripEndTime")
    @Expose
    private long tripEndTime;
    @SerializedName("routeNo")
    @Expose
    private String routeNo;
    @SerializedName("routeName")
    @Expose
    private String routeName;
    @SerializedName("firstStopId")
    @Expose
    private Integer firstStopId;
    @SerializedName("firstStopCode")
    @Expose
    private String firstStopCode;
    @SerializedName("firstStopName")
    @Expose
    private String firstStopName;
    @SerializedName("lastStopId")
    @Expose
    private Integer lastStopId;
    @SerializedName("lastStopCode")
    @Expose
    private String lastStopCode;
    @SerializedName("lastStopName")
    @Expose
    private String lastStopName;
    @SerializedName("createdOn")
    @Expose
    private long createdOn;

    public Integer getTripId() {
        return tripId;
    }

    public void setTripId(Integer tripId) {
        this.tripId = tripId;
    }

    public Integer getTripNo() {
        return tripNo;
    }

    public void setTripNo(Integer tripNo) {
        this.tripNo = tripNo;
    }

    public String getTripDirection() {
        return tripDirection;
    }

    public void setTripDirection(String tripDirection) {
        this.tripDirection = tripDirection;
    }

    public long getTripStartTime() {
        return tripStartTime;
    }

    public void setTripStartTime(Integer tripStartTime) {
        this.tripStartTime = tripStartTime;
    }

    public long getTripEndTime() {
        return tripEndTime;
    }

    public void setTripEndTime(Integer tripEndTime) {
        this.tripEndTime = tripEndTime;
    }

    public String getRouteNo() {
        return routeNo;
    }

    public void setRouteNo(String routeNo) {
        this.routeNo = routeNo;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public Integer getFirstStopId() {
        return firstStopId;
    }

    public void setFirstStopId(Integer firstStopId) {
        this.firstStopId = firstStopId;
    }

    public String getFirstStopCode() {
        return firstStopCode;
    }

    public void setFirstStopCode(String firstStopCode) {
        this.firstStopCode = firstStopCode;
    }

    public String getFirstStopName() {
        return firstStopName;
    }

    public void setFirstStopName(String firstStopName) {
        this.firstStopName = firstStopName;
    }

    public Integer getLastStopId() {
        return lastStopId;
    }

    public void setLastStopId(Integer lastStopId) {
        this.lastStopId = lastStopId;
    }

    public String getLastStopCode() {
        return lastStopCode;
    }

    public void setLastStopCode(String lastStopCode) {
        this.lastStopCode = lastStopCode;
    }

    public String getLastStopName() {
        return lastStopName;
    }

    public void setLastStopName(String lastStopName) {
        this.lastStopName = lastStopName;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Integer createdOn) {
        this.createdOn = createdOn;
    }

}
