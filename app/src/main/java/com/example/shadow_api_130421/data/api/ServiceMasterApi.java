package com.example.shadow_api_130421.data.api;

import com.example.shadow_api_130421.data.models.login.LoginDepot;
import com.example.shadow_api_130421.data.models.service.ServiceTypeMaster;
import com.example.shadow_api_130421.data.models.service4.ServiceTypeFour;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ServiceMasterApi {

    @GET("/loadServiceTypeMaster")
    Call<List<ServiceTypeMaster>> getServicesMasterType(@Query("lastSyncTime") int lastSyncTime,
                                                        @Header("Authorization") String authHeader);

    @POST("/login")
    Call<LoginDepot> justLogin(@Query("username") String username,
                               @Query("password") String password,
                               @Header("Authorization") String authHeader);

    @GET("/loadServiceMaster")
    Call<List<ServiceTypeFour>> tripService(@Query("serviceTypeId") int serviceTypeId,
                                            @Query("imei") String imei,
                                            @Header("Authorization") String authHeader);

}
