package com.example.shadow_api_130421.data.models.service;

public class ExtraTaxes {

    private int taxId;

    private String taxName;

    private String taxCode;

    private double taxValue;

    private long createdOn;

    public int getTaxId() {
        return taxId;
    }

    public String getTaxName() {
        return taxName;
    }

    public String getTaxCode() {
        return taxCode;
    }

    public double getTaxValue() {
        return taxValue;
    }

    public long getCreatedOn() {
        return createdOn;
    }
}
