package com.example.shadow_api_130421.data.models.service;

import java.util.List;

public class ServiceTypeMaster {

    private int serviceTypeId;

    private String serviceTypeCode;

    private String serviceTypeName;

    private boolean catTicketAllowed;

    private int roundOffToNextMultiple;

    private int fareMatrixCategoryId;

    private double basicFare;

    private double minimumFare;

    private long createdOn;

    private List<ConcessionMasters> concessionMasters;

    private List<FlatFareMasters> flatFareMasters;

    private List<TicketTypes> ticketTypes;

    private List<ExtraTaxes> extraTaxes;

    private List<Integer> luggageFares;

    private FareMatrixMap fareMatrixMap;

    public int getServiceTypeId() {
        return serviceTypeId;
    }

    public String getServiceTypeCode() {
        return serviceTypeCode;
    }

    public String getServiceTypeName() {
        return serviceTypeName;
    }

    public boolean isCatTicketAllowed() {
        return catTicketAllowed;
    }

    public int getRoundOffToNextMultiple() {
        return roundOffToNextMultiple;
    }

    public int getFareMatrixCategoryId() {
        return fareMatrixCategoryId;
    }

    public double getBasicFare() {
        return basicFare;
    }

    public double getMinimumFare() {
        return minimumFare;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public List<ConcessionMasters> getConcessionMasters() {
        return concessionMasters;
    }

    public List<FlatFareMasters> getFlatFareMasters() {
        return flatFareMasters;
    }

    public List<TicketTypes> getTicketTypes() {
        return ticketTypes;
    }

    public List<ExtraTaxes> getExtraTaxes() {
        return extraTaxes;
    }

    public List<Integer> getLuggageFares() {
        return luggageFares;
    }

    public FareMatrixMap getFareMatrixMap() {
        return fareMatrixMap;
    }
}
