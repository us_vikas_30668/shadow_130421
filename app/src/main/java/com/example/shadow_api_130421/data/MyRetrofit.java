package com.example.shadow_api_130421.data;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyRetrofit {

    static Retrofit myRetrofit;

    public MyRetrofit() {
        myRetrofit = new Retrofit.Builder()
                .baseUrl("http://staging-api-afcsap.chalo.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static Retrofit getMyRetrofit() {
        return myRetrofit;
    }
}
