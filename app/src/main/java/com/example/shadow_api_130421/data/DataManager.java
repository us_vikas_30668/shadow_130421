package com.example.shadow_api_130421.data;

import android.util.Log;

import com.example.shadow_api_130421.data.api.ServiceMasterApi;
import com.example.shadow_api_130421.data.models.login.LoginDepot;
import com.example.shadow_api_130421.data.models.service.ServiceTypeMaster;
import com.example.shadow_api_130421.data.models.service4.ServiceTypeFour;
import com.example.shadow_api_130421.ui.login.LoginMvpModel;
import com.example.shadow_api_130421.ui.main.MainMvpModel;
import com.example.shadow_api_130421.ui.servicefour.ServiceMvpModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Service
 * -> concessionMasters
 * -> flatFareMasters
 * -> ticketTypes
 * -> extraTaxes
 * -> fareMatrixMap
 */

public class DataManager implements MainMvpModel, LoginMvpModel, ServiceMvpModel {

    private static Retrofit retrofit;
    private static ServiceMasterApi serviceMasterApi;
    private static final String BEARER_TOKEN = "Bearer eyJhbGciOiJIUzI1NiIsInppcCI6IkdaSVAifQ.H4sIAAAAAAAAAKtWyiwuVrJSUtJRykwsUbIyNDO0MLI0MLYw1VFKrSiACBgbmYEFSotTi_ISc1OB6ktSi0uUagGQQkuQPgAAAA.XXv2s0kfOopiDJJg4AuEmCarH5ZFyaB5Wr_9NRwnLhY";

    private static final String LOG_TAG = "sakivG";

    public DataManager() {
        MyRetrofit myRetrofit = new MyRetrofit();
        retrofit = MyRetrofit.getMyRetrofit();

        Log.v(LOG_TAG, myRetrofit.toString());

        if (myRetrofit != null) {
            serviceMasterApi = retrofit.create(ServiceMasterApi.class);
        }
    }

    @Override
    public void getServiceTypesMaster(MainMvpModel.OnFinishedListener onFinishedListener, int lastSyncTime) {

        Call<List<ServiceTypeMaster>> serviceTypesMasterCall = serviceMasterApi == null
                ? null : serviceMasterApi.getServicesMasterType(lastSyncTime,
                BEARER_TOKEN);

        Log.v(LOG_TAG, serviceTypesMasterCall.toString());

        if (serviceTypesMasterCall == null) {
            return;
        }

        serviceTypesMasterCall.enqueue(new Callback<List<ServiceTypeMaster>>() {
            @Override
            public void onResponse(Call<List<ServiceTypeMaster>> call, Response<List<ServiceTypeMaster>> response) {

                Log.v(LOG_TAG, response.body() == null ? "NULL BODY" :
                        response.body().toString());
                List<ServiceTypeMaster> list = response.body();
                onFinishedListener.onFinished(list);
            }

            @Override
            public void onFailure(Call<List<ServiceTypeMaster>> call, Throwable t) {

                Log.v(LOG_TAG, t.getMessage());
                onFinishedListener.onFailure(t);
            }
        });
    }


    @Override
    public void getLoggedDepot(OnFinishedLoginListener onFinishedLoginListener, String username, String password) {
        Call<LoginDepot> loginCall = serviceMasterApi == null
                ? null : serviceMasterApi.justLogin(username, password, BEARER_TOKEN);

        if(loginCall == null)
            return;

        loginCall.enqueue(new Callback<LoginDepot>() {
            @Override
            public void onResponse(Call<LoginDepot> call, Response<LoginDepot> response) {
                LoginDepot loginDepot = response.body();
                onFinishedLoginListener.onLoginFinished(loginDepot);
            }

            @Override
            public void onFailure(Call<LoginDepot> call, Throwable t) {
                onFinishedLoginListener.onLoginFailure(t);
            }
        });

    }

    @Override
    public void getServiceFourList(OnFinishedServiceFourListener onFinishedServiceFourListener, int serviceType, String imei) {
        Call<List<ServiceTypeFour>> serviceFourCall = serviceMasterApi == null
                ? null : serviceMasterApi.tripService(serviceType, imei, BEARER_TOKEN);

        if(serviceFourCall == null)
            return;

        serviceFourCall.enqueue(new Callback<List<ServiceTypeFour>>() {
            @Override
            public void onResponse(Call<List<ServiceTypeFour>> call, Response<List<ServiceTypeFour>> response) {
                List<ServiceTypeFour> list = response.body();

                Log.v("skaivg", list.toString());

                onFinishedServiceFourListener.onServiceFourFinished(list);
            }

            @Override
            public void onFailure(Call<List<ServiceTypeFour>> call, Throwable t) {
                onFinishedServiceFourListener.onServiceFourFailure(t);
            }
        });
    }
}
