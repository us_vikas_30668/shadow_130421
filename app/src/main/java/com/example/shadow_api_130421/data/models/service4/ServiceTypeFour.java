
package com.example.shadow_api_130421.data.models.service4;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class ServiceTypeFour {

    @SerializedName("serviceId")
    @Expose
    private Integer serviceId;
    @SerializedName("serviceTypeId")
    @Expose
    private Integer serviceTypeId;
    @SerializedName("serviceName")
    @Expose
    private String serviceName;
    @SerializedName("serviceNumber")
    @Expose
    private String serviceNumber;
    @SerializedName("serviceType")
    @Expose
    private String serviceType;
    @SerializedName("starTime")
    @Expose
    private long starTime;
    @SerializedName("endTime")
    @Expose
    private long endTime;
    @SerializedName("createdOn")
    @Expose
    private long createdOn;
    @SerializedName("tripMasters")
    @Expose
    private List<TripMaster> tripMasters = null;
    @SerializedName("fareMatrixIds")
    @Expose
    private List<Integer> fareMatrixIds = null;

    public Integer getServiceId() {
        return serviceId;
    }

    public void setServiceId(Integer serviceId) {
        this.serviceId = serviceId;
    }

    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceNumber() {
        return serviceNumber;
    }

    public void setServiceNumber(String serviceNumber) {
        this.serviceNumber = serviceNumber;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public long getStarTime() {
        return starTime;
    }

    public void setStarTime(Integer starTime) {
        this.starTime = starTime;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(Integer endTime) {
        this.endTime = endTime;
    }

    public long getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Integer createdOn) {
        this.createdOn = createdOn;
    }

    public List<TripMaster> getTripMasters() {
        return tripMasters;
    }

    public void setTripMasters(List<TripMaster> tripMasters) {
        this.tripMasters = tripMasters;
    }

    public List<Integer> getFareMatrixIds() {
        return fareMatrixIds;
    }

    public void setFareMatrixIds(List<Integer> fareMatrixIds) {
        this.fareMatrixIds = fareMatrixIds;
    }

}
