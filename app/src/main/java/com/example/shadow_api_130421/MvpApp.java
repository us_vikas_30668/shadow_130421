package com.example.shadow_api_130421;

import android.app.Application;

import com.example.shadow_api_130421.data.DataManager;

public class MvpApp extends Application {

    DataManager dataManager;

    @Override
    public void onCreate() {
        super.onCreate();

        dataManager = new DataManager();
    }

    public DataManager getDataManager() {
        return dataManager;
    }

}
