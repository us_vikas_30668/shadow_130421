package com.example.shadow_api_130421.ui.login;

import com.example.shadow_api_130421.ui.base.MvpView;

public interface LoginMvpView extends MvpView {

    void setData(String text);
}
