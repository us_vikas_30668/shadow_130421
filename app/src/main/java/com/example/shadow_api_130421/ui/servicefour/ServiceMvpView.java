package com.example.shadow_api_130421.ui.servicefour;

import com.example.shadow_api_130421.ui.base.MvpView;

public interface ServiceMvpView extends MvpView {

    void setData(String data);
}
