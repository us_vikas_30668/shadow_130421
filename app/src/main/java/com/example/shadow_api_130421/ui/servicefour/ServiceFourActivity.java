package com.example.shadow_api_130421.ui.servicefour;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.shadow_api_130421.MvpApp;
import com.example.shadow_api_130421.R;
import com.example.shadow_api_130421.data.DataManager;

public class ServiceFourActivity extends AppCompatActivity implements ServiceMvpView{

    private ServicePresenter servicePresenter;

    private Button button;

    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_four);

        DataManager dataManager = ((MvpApp) getApplicationContext()).getDataManager();

        servicePresenter = new ServicePresenter(dataManager);

        servicePresenter.onAttach(this);

        button = findViewById(R.id.button_trip);
        textView = findViewById(R.id.text_view_trip);

        button.setOnClickListener(v -> servicePresenter.fetchServiceFourList());
    }

    @Override
    public void setData(String data) {
        textView.setText(data);
    }
}