package com.example.shadow_api_130421.ui.main;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.shadow_api_130421.MvpApp;
import com.example.shadow_api_130421.R;
import com.example.shadow_api_130421.data.DataManager;
import com.example.shadow_api_130421.ui.login.LoginActivity;
import com.example.shadow_api_130421.ui.servicefour.ServiceFourActivity;

public class MainActivity extends AppCompatActivity implements MainMvpView{

    MainPresenter mainPresenter;

    TextView textView;

    Button button, buttonService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.text_view_length);

        button = findViewById(R.id.button_gotoLogin);

        buttonService = findViewById(R.id.button_gotoServiceFour);

        MvpApp mvpApp = (MvpApp) getApplicationContext();
        DataManager dataManager = mvpApp.getDataManager();

        mainPresenter = new MainPresenter(dataManager);
        mainPresenter.onAttach(this);

        mainPresenter.fetchServiceTypesMaster();

        button.setOnClickListener(v -> openLoginActivity(getApplicationContext()));

        buttonService.setOnClickListener(v -> openServiceFourActivity(getApplicationContext()));
    }

    @Override
    public void setData(String data) {
        textView.setText(data);
    }

    @Override
    public void openLoginActivity(Context context) {
        startActivity(new Intent(context, LoginActivity.class));
    }

    @Override
    public void openServiceFourActivity(Context context) {
        startActivity(new Intent(context, ServiceFourActivity.class));
    }
}