package com.example.shadow_api_130421.ui.servicefour;

import com.example.shadow_api_130421.data.DataManager;
import com.example.shadow_api_130421.data.models.service4.ServiceTypeFour;
import com.example.shadow_api_130421.ui.base.BasePresenter;

import java.util.List;

public class ServicePresenter<V extends ServiceMvpView> extends BasePresenter<V> implements
        ServiceMvpPresenter<V>,
        ServiceMvpModel.OnFinishedServiceFourListener{

    public ServicePresenter(DataManager dataManager) {
        super(dataManager);
    }


    @Override
    public void fetchServiceFourList() {
        getDataManager().getServiceFourList(this, 4, "865543040108527");
    }

    @Override
    public void onServiceFourFinished(List<ServiceTypeFour> serviceTypeFourList) {
        if(serviceTypeFourList == null)
            getView().setData("Could not fetch data");

        String data = "";
        for(ServiceTypeFour serviceTypeFour: serviceTypeFourList){
            data += "\n Service Type = " + serviceTypeFour.getServiceType()
                    + "\n Service name = " + serviceTypeFour.getServiceName();
        }
        getView().setData(data);
    }

    @Override
    public void onServiceFourFailure(Throwable t) {
        getView().setData(t.getMessage());
    }
}
