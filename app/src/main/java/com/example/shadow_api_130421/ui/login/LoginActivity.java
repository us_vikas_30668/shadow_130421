package com.example.shadow_api_130421.ui.login;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.shadow_api_130421.MvpApp;
import com.example.shadow_api_130421.R;
import com.example.shadow_api_130421.data.DataManager;

public class LoginActivity extends AppCompatActivity implements LoginMvpView{

    private LoginPresenter loginPresenter;
    private DataManager dataManager;

    private Button button;
    private TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        dataManager = ((MvpApp)getApplicationContext()).getDataManager();

        loginPresenter = new LoginPresenter(dataManager);
        loginPresenter.onAttach(this);

        button = findViewById(R.id.button_login);
        textView = findViewById(R.id.text_view_loginMember);

        button.setOnClickListener(v -> loginPresenter.login());
    }

    @Override
    public void setData(String text) {
        textView.setText(text);
    }
}