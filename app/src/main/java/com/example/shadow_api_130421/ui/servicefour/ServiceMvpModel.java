package com.example.shadow_api_130421.ui.servicefour;

import com.example.shadow_api_130421.data.models.login.LoginDepot;
import com.example.shadow_api_130421.data.models.service4.ServiceTypeFour;
import com.example.shadow_api_130421.ui.login.LoginMvpModel;

import java.util.List;

public interface ServiceMvpModel {

    interface OnFinishedServiceFourListener {
        void onServiceFourFinished(List<ServiceTypeFour> serviceTypeFourList);

        void onServiceFourFailure(Throwable t);
    }

    void getServiceFourList(OnFinishedServiceFourListener onFinishedServiceFourListener, int serviceType, String imei);
}
