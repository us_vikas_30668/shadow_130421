package com.example.shadow_api_130421.ui.main;

import com.example.shadow_api_130421.data.models.service.ServiceTypeMaster;

import java.util.List;

public interface MainMvpModel {

    interface OnFinishedListener {
        void onFinished(List<ServiceTypeMaster> list);

        void onFailure(Throwable t);
    }

    void getServiceTypesMaster(OnFinishedListener onFinishedListener, int lastTimeSync);
}
