package com.example.shadow_api_130421.ui.main;

import com.example.shadow_api_130421.data.DataManager;
import com.example.shadow_api_130421.ui.base.BasePresenter;
import com.example.shadow_api_130421.data.models.service.ServiceTypeMaster;

import java.util.List;

public class MainPresenter<V extends MainMvpView> extends BasePresenter<V> implements MainMvpPresenter<V>, MainMvpModel.OnFinishedListener {

    public MainPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void fetchServiceTypesMaster() {
        getDataManager().getServiceTypesMaster(this, 0);
    }

    @Override
    public void onFinished(List<ServiceTypeMaster> list) {
        if(list == null)
            getView().setData("No data found");
        else{
            String data = "";
            for(ServiceTypeMaster type: list){
                data += "\n" + type.getServiceTypeName();
            }
            getView().setData(data);
        }
    }

    @Override
    public void onFailure(Throwable t) {
        getView().setData(t.getMessage());
    }
}
