package com.example.shadow_api_130421.ui.login;

import com.example.shadow_api_130421.data.models.login.LoginDepot;
import com.example.shadow_api_130421.data.models.service.ServiceTypeMaster;
import com.example.shadow_api_130421.ui.main.MainMvpModel;

import java.util.List;

public interface LoginMvpModel {

    interface OnFinishedLoginListener {
        void onLoginFinished(LoginDepot loginDepot);

        void onLoginFailure(Throwable t);
    }

    void getLoggedDepot(OnFinishedLoginListener onFinishedLoginListener, String username, String password);
}
