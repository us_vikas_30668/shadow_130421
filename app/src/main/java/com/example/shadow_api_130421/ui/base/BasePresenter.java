package com.example.shadow_api_130421.ui.base;

import com.example.shadow_api_130421.data.DataManager;

public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    private V view;

    DataManager dataManager;

    public BasePresenter(DataManager dataManager) {
        this.dataManager = dataManager;
    }

    @Override
    public void onAttach(V view) {
        this.view = view;
    }

    public V getView() {
        return view;
    }

    public DataManager getDataManager() {
        return dataManager;
    }
}
