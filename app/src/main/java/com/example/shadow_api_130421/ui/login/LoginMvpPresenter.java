package com.example.shadow_api_130421.ui.login;

public interface LoginMvpPresenter<V extends LoginMvpView> {

    void login();
}
