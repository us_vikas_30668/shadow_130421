package com.example.shadow_api_130421.ui.main;

public interface MainMvpPresenter<V extends MainMvpView>  {

    void fetchServiceTypesMaster();
}
