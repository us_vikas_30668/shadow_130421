package com.example.shadow_api_130421.ui.servicefour;

public interface ServiceMvpPresenter<V extends ServiceMvpView> {

    void fetchServiceFourList();
}
