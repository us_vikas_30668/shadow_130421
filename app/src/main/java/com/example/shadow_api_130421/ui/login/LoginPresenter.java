package com.example.shadow_api_130421.ui.login;

import com.example.shadow_api_130421.data.DataManager;
import com.example.shadow_api_130421.data.models.login.LoginDepot;
import com.example.shadow_api_130421.ui.base.BasePresenter;

public class LoginPresenter<V extends LoginMvpView> extends BasePresenter<V> implements LoginMvpPresenter<V>, LoginMvpModel.OnFinishedLoginListener {

    public LoginPresenter(DataManager dataManager) {
        super(dataManager);
    }

    @Override
    public void login() {
        getDataManager().getLoggedDepot(this, "test", "hrhk");
    }

    @Override
    public void onLoginFinished(LoginDepot loginDepot) {
        if(loginDepot == null)
            getView().setData("Unauthenticated ..");

        String data = "";

        data += "\nName = " + loginDepot.getDisplayname()
                + "\nEmail = " + loginDepot.getEmail()
                + "\nDepot Id = " + loginDepot.getDepotId()
                + "\nDepot name = " + loginDepot.getDepotName();

        getView().setData(data);
    }

    @Override
    public void onLoginFailure(Throwable t) {
        getView().setData(t.getMessage());
    }
}
