package com.example.shadow_api_130421.ui.base;

import com.example.shadow_api_130421.ui.base.MvpView;

public interface MvpPresenter<V extends MvpView> {

    void onAttach(V view);
}
