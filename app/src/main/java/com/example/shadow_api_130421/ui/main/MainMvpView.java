package com.example.shadow_api_130421.ui.main;

import android.content.Context;

import com.example.shadow_api_130421.ui.base.MvpView;

public interface MainMvpView extends MvpView {

    void setData(String data);

    void openLoginActivity(Context context);

    void openServiceFourActivity(Context context);
}
